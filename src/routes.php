<?php
use App\Controller\AttendanceController;

$app->group('/api', function () use ($app) {
    $app->post('/all', AttendanceController::class . ':getallusers');
    $app->post('/login', AttendanceController::class . ':login');
    $app->post('/user/status', AttendanceController::class . ':user_status');
    $app->post('/user/info', AttendanceController::class . ':showUserInfo');
});
