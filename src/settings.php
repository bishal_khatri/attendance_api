<?php
return [
    'settings' => [
        'displayErrorDetails' => true, // set to false in production
        'addContentLengthHeader' => false, // Allow the web server to send the content-length header
        'routes.case_sensitive' => false,

        // Renderer settings
        'renderer' => [
            'template_path' => __DIR__ . '/../templates/',
        ],

        // Monolog settings
        'logger' => [
            'name' => 'slim-app',
            'path' => isset($_ENV['docker']) ? 'php://stdout' : __DIR__ . '/../logs/app.log',
            'level' => \Monolog\Logger::DEBUG,
        ],

        // Database connection settings
        'db' => [
            'driver' => 'mysql',
            'host' => '172.16.1.140',
//            'host' => '192.168.10.17',
            'port' => '83306',
            'database' => 'adms_db',
//            'username' => 'nepal',
//            'password' => 'technology@123',
            'username' => 'technology',
            'password' => 'technology',
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
        ],

        date_default_timezone_set ('Asia/Kathmandu'),
    ],
];
