<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UserDepartment extends Model
{
    protected $table = 'iclock_deptadmin';
    public $timestamps = false;
}
