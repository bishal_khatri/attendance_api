<?php
/**
 * Created by PhpStorm.
 * User: Lenovo
 * Date: 3/3/2017
 * Time: 1:13 PM
 */

namespace App;


class Helper
{
    public static  function setResponse($type, $message, $responseData, $responseMetaData = [])
    {
        $error = '';
        if ($type === 'success') {
            $error = false;
        }

        if ($type === 'error') {
            $error = true;
        }

        return [
            'error' => $error,
            'message' => $message,
            'data' => $responseData,
            'metaData' => $responseMetaData
        ];
    }


   public static function cmp($a, $b) {
        if ($a['checkin'] == $b['checkin']) {
            return 0;
        }
        return ($a['checkin'] < $b['checkin']) ? 1 : -1;
    }


}