<?php
namespace App\Controller;

use App\Helper;
use App\Model\Checkinout;
use App\Model\Device;
use App\Model\Department;
use App\Model\RouteStation;
use App\Model\User;
use App\Model\Userinfo;
use DateTime;
use Firebase\JWT\JWT;
use phpDocumentor\Reflection\Types\Self_;
use Psr\Container\ContainerInterface as Container;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Tuupola\Base62;
use Illuminate\Database\Capsule\Manager as DB;
use App\Model\UserDepartment;

class AttendanceController
{

    /**
     * RouteController constructor.
     *
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
        $this->validator = $container['validator'];
        $this->logger = $container['logger'];
        $this->userLevel = 0;

        if (isset($container->token)) {
            $this->token = $container->token;
        }
    }

    public function createToken($data)
    {

        //create token
        $now = new DateTime();
        $future = new DateTime("now +365 days");
        $jti = Base62::encode(random_bytes(16));
        $payload = [
            "iat" => $now->getTimeStamp(),
            "exp" => $future->getTimeStamp(),
            "jti" => $jti,
            "userid"=>$data[0]
        ];
        $secret = getenv("JWT_SECRET");
        $token = JWT::encode($payload, $secret, "HS256");

        return $token;
    }

    public function login(Request $request, Response $response)
    {
        $rawData = $request->getParsedBody();

        // validation
        $this->validator->validation_rules(array(
            'username' => 'required',
            'password' => 'required',
        ));

        $validatedData = $this->validator->run($request->getParsedBody());

        if($validatedData === false){
            return $response->withJson(Helper::setResponse('error', 'Missing Parameters.', ''));
        }

        //check username exist or not in db.
        $user = User::where('username',$validatedData['username'])->first();
        if($user){
            $salt = substr($user->password,5,5);
            $sha1 = sha1($salt.$validatedData['password']);
            $password = 'sha1$'.$salt.'$'.$sha1;
            if($password == $user->password){
                /*$token = Self::createToken($user);
                $data['token'] = $token;
                $userDepartment = UserDepartment::where('user_id',$user->id)->pluck("dept_id");*/

                //modification from here.
                //if user id admin. it's id is always 1.
                if($user->id == 1){
                    $userDepartment[0] = 1;
                    $token = Self::createToken($userDepartment);
                    $data['token'] = $token;
                }
                else {
                    //check whether the authenticated user is expired or not.

                    $today_date = new DateTime(date('Y-m-d H:i:s'));
                    $expiry_date = new DateTime($user->expiry_date);

                    if ($today_date > $expiry_date)
                    {
                        $responseData = Helper::setResponse('error', 'User Subscription is expired. Please contact Administrator.', '', '');
                        return $response->withJson($responseData);
                    }

                    $userDepartment = UserDepartment::where('user_id',$user->id)->pluck("dept_id");
                    $token = Self::createToken($userDepartment);
                    $data['token'] = $token;
                }




                //retrieving authenticated user granted department
                $granted_department = DB::table('departments')->where('DeptID',$userDepartment)->first();
                $data['department'] = $granted_department;



                $responseData = Helper::setResponse('success', 'Login Successful', $data,'');
                return $response->withJson($responseData);
            }
            else{
                return $response->withJson(Helper::setResponse('error', 'Password doesnot match', ''));
            }
        }
        else{
            return $response->withJson(Helper::setResponse('error', 'Username not found', ''));
        }
    }

    public function getallusers(Request $request, Response $response)
    {
        if (isset($this->container->token)) {
           $userid = $this->container->token->userid;
        }
        $rawData = $request->getParsedBody();

        // validation
        $this->validator->validation_rules(array(
            'date' => 'required',
        ));

        if(isset($rawData['child_id'])){
            $userid = $rawData['child_id'];
        }


        $validatedData = $this->validator->run($rawData);

        if(!$validatedData){
            return $response->withJson(Helper::setResponse('error', 'Missing Parameters.', ''));
        }

        //taking tomorrow date from post user date
        $timestamp = strtotime($rawData['date'].' + 1 day');
        $next_date = date('Y-m-d',$timestamp);

        // checking date for a status and change in function.
        if(date('Y-m-d') == $rawData['date']){
            $date_status = 1;
        }
        else{
            $date_status = 0;
        }

//        $userDepartment = UserDepartment::where('user_id',$this->container->token->userid)->pluck("dept_id");

        $device = Device::where('DeptID',$userid)->get();
//dd($device);
        $devices_status = array();
        foreach ($device as $value){
            $data ['Alias'] = $value->Alias;
            $data ['device_name'] = $value->SN;
            $data ['last_activity'] = $value->LastActivity;
            $data ['dept_id'] = $value->DeptID;
            $data['server_time'] = date('Y-m-d H:i:s');
            array_push($devices_status,$data);
        }

        //initializing the count to 0
        $absent = 0;
        $checkout = 0;
        $present = 0;

        $parent = Department::where('DeptID',$userid)->first();
        $parentUser = Userinfo::where('defaultdeptid',$parent->DeptID)->get();
        $child = Department::where('supdeptid',$parent->DeptID)->get();

        foreach ($parentUser as $value){
            $att = Checkinout::where('userid',$value->userid)->whereBetween('checktime', [$validatedData['date'], $next_date])->get();

            if ($att->count()<1){
                $absent ++;
            }
            else{
                if($date_status == 1){
                    $count = $att->count();
                    if ($count % 2 == 0){
                        $checkout ++;
                    }
                    else{
                        $present ++;
                    }
                }
                else {
                    $present ++;
                }
            }
        }

        //placing them in a format
        $data = array();
        //if date given by user is today
        if($date_status == 1) {
            $data['parent'] = [
                'department'=>$parent,
                'attendance' => array(
                    'present' => $present,
                    'checkout'=>$checkout,
                    'absent' =>$absent,
                    'total' => $present + $checkout + $absent
                )
            ];
        }

        //if date given by user is previous date no need to show checkout.
        else{
            $data['parent'] = [
                'department'=>$parent,
                'attendance' => array(
                    'present' => $present,
                    'absent' =>$absent,
                    'checkout' => null,
                    'total' => $absent + $present
                )
            ];
        }

        $data['child'] = [
            'department'=>$child
        ];

        $data['device_status'] = $devices_status;
        $responseData = Helper::setResponse('success', 'Listing All User Successfull', $data,'');

        return $response->withJson($responseData);
    }

    public function user_status(Request $request, Response $response)
    {
        if (isset($this->container->token)) {
            $userid = $this->container->token->userid;
        }

        $rawData = $request->getParsedBody();
        // validation
        $this->validator->validation_rules(array(
            'date' => 'required'
        ));

        if (isset($rawData['child_id'])) {
            $userid = $rawData['child_id'];
        }

        $validatedData = $this->validator->run($rawData);
        if (!$validatedData) {
            return $response->withJson(Helper::setResponse('error', 'Missing Parameters.', ''));
        }

        //taking tomorrow date from post user date
        $timestamp = strtotime($rawData['date'] . ' + 1 day');
        $next_date = date('Y-m-d', $timestamp);

        // to retrieve the department name and id.
//        $userDepartment = UserDepartment::where('user_id',$userid)->pluck("dept_id");
        $parent = Department::where('DeptID', $userid)->first();
        // dd($parent);
        $absent = array();
        $present = array();

        $parentUser = Userinfo::where('defaultdeptid', $parent->DeptID)->get();
        foreach ($parentUser as $value) {
            $data = array();

            //retrieving all checktime of a user input date of a user.
            $att = Checkinout::where('userid', $value->userid)->whereBetween('checktime', [$validatedData['date'], $next_date])->get();
            //if count is 0 i.e. absent.
            if ($att->count() < 1) {
                $data['name'] = $value->name;
                $data['badgenumber'] = $value->badgenumber;
//                $data['userid'] = $value->userid;
                //adding data to absent array.
                array_push($absent, $data);
            } //if count is greater than 2 0r more i.e checkout or present.
            else {
                //retrieving the first checkin of a user of user input date.
                $att_first = Checkinout::where('userid', $value->userid)->whereBetween('checktime', [$validatedData['date'], $next_date])->orderBy('checktime', 'asc')->first();
                $att_last = Checkinout::where('userid', $value->userid)->whereBetween('checktime', [$validatedData['date'], $next_date])->orderBy('checktime', 'desc')->first();

//                $data['userid'] = $value->userid;
                $data['name'] = $value->name;
                $data['badgenumber'] = $value->badgenumber;
                $data['checkin'] = $att_first->checktime;

                //if today is not selected
                if ($att->count() == 1) {
                    $data['checkout'] = null;
                } else {
                    $data['checkout'] = $att_last->checktime;
                }
                array_push($present, $data);
            }
        }

        //sorting them on basis of check time.
        uasort($present, array('App\Helper', 'cmp'));
        foreach ($present as $value) {
            $p[] = $value;
        }

        $data = array();
        $data['parent'] = [
            'department' => $parent,
            'present' => $p,
            'absent' => $absent
        ];

        $responseData = Helper::setResponse('success', 'Listing All User Successfull', $data, '');
        return $response->withJson($responseData);
    }

    public function showUserInfo(Request $request, Response $response){

        $rawData = $request->getParsedBody();
        // validation
        $this->validator->validation_rules(array(
            'start_date' => 'required',
            'end_date' => 'required',
            'badgenumber' => 'required',
        ));

        $badgenumber = $rawData['badgenumber'];

        $end_date_next = date('Y-m-d', strtotime($rawData['end_date'] . ' + 1 day'));

        if(date('Y-m-d') >= $rawData['start_date']){
            $start_date = $rawData['start_date'];

        }
        else{
            $start_date = date('Y-m-d');

        }

        if(date('Y-m-d') >= $rawData['end_date']){
            $end_date = $end_date_next;

        }
        else{
            $end_date = date("Y-m-d", time() + 86400);

        }
        // calculating difference between two dates in days.

        $date1 = strtotime($end_date);
        $date2 = strtotime($start_date);
        $diff_days = round(($date1 - $date2)/(60*60*24));


        $validatedData = $this->validator->run($rawData);
        if (!$validatedData) {
            return $response->withJson(Helper::setResponse('error', 'Missing Parameters.', ''));
        }

        //retrieving user information
        $user = Userinfo::where('badgenumber', $badgenumber)->first(['userid','badgenumber','defaultdeptid','name']);

        //if user is not found
        if(empty($user)){
            $responseData = Helper::setResponse('error', 'User not found', '', '');
            return $response->withJson($responseData);
        }


        $temp_date = $start_date ;
        $info = array();

        for ($i= 0; $i < $diff_days; $i++) {

            $data = array();
            $date_next = date('Y-m-d', strtotime($temp_date . ' + 1 day'));


            //retrieving all checktime of a user input date of a user.
            $att = Checkinout::where('userid', $user->userid)->whereBetween('checktime', [$temp_date, $date_next])->get();

            //if count is 0 i.e. absent.
            if ($att->count() < 1) {

                $data['status'] = 'absent';
                $data['date'] = $temp_date;

                //adding absent  data to info array.
                array_push($info, $data);

            }

            //if count is greater than 2 0r more i.e checkout or present.
            else {

                //retrieving the first and last checkin of a user from user input date.
                $att_first = Checkinout::where('userid', $user->userid)->whereBetween('checktime', [$temp_date, $date_next])->orderBy('checktime', 'asc')->first();
                $att_last = Checkinout::where('userid', $user->userid)->whereBetween('checktime', [$temp_date, $date_next])->orderBy('checktime', 'desc')->first();

                $data['status'] = 'present';
                $data['date'] = $temp_date;
                $data['checkin'] = $att_first->checktime;

                //if today is not selected
                if ($att->count() == 1) {
                    $data['checkout'] = null;
                } else {
                    $data['checkout'] = $att_last->checktime;
                }
                array_push($info, $data);
            }
            $temp_date = date('Y-m-d', strtotime($temp_date . ' + 1 day'));

        }

        if(empty($info)){
            $info = null;
        }

        $data = [
            'user' => $user,
            'info' => $info,
        ];

        $responseData = Helper::setResponse('success', 'Listing  User Information Based On Two Dates Successfull ', $data, '');
        return $response->withJson($responseData);

    }
}
