<?php
use App\RouteController;
use App\Helper;
use Slim\Middleware\JwtAuthentication;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

// Application middleware
// e.g: $app->add(new \Slim\Csrf\Guard);

$container = $app->getContainer();

$container["jwt"] = function ($container) {
    return new StdClass;
};

//Authentication middleware
$app->add(new JwtAuthentication([
    "path" => "/",
    "passthrough" => [
        "/api/login"
    ],
    "secret" => getenv("JWT_SECRET"),
    "relaxed" => ["localhost", "172.16.1.140","202.52.240.148"],
    "error" => function ($request, $response, $args) use ($container) {
        return $response->withJson(Helper::setResponse('error', $args["message"], ''));
    },
    "callback" => function ($request, $response, $args) use ($container) {
        //make the token available for further use
        $container["token"] = $args["decoded"];
    }
]));
